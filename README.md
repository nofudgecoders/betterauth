# BetterAuth #

* What is BetterAuth?
* Why is BetterAuth "better"?
* Features
* Philosophy
* Definitions
* Data transmission
* Used algorithms

## What is BetterAuth? ##

BetterAuth is a way to authenticate in a network, without worrying about to relay personal data to services and 3rd parties.
Today's authentication services requires you to insert your personal data, if you like it or not. Companies, governments and non-reputable services tend to abuse your personal data.

## Why is BetterAuth "better"? ##

BetterAuth allows users to generate persistent and transient identifications, which also can be used to export or proxy identifications.

## Features ##

* Persistent and transient identifications
* Ability to host identification authorities
* Export identifications
* Use proxy services to authentificate without giving out real identifications (onion system)
* Plans to host an official identification authority

## Philosophy ##

Every message has to be kept secret (except in non encryption mode) between the sender and the trusted receiver.
Authorities do keep track of private keys, but can't decrypt messages completly (partial cross decryption)
Services won't be able to decrypt completly, unless an authority partially decrypted the message, thus also allowing authorities to tell, if this is a legit identification.

1. A client sends a session request, for identification purposes at the service.
2. The service sends the encrypted message of the client to the authority.
3. The authority checks the validity and replies to the service with the partially decrypted message.
4. The service finalizes the decryption of the partially decrypted message, and decides whether to trust or not the identification. If trusted, send a encrypted session key to the client.
5. The client checks at its authority, whether the service has actually asked this authority. If everything is alright, the BetterAuth session key is valid and the client agrees to the service.

## Definitions ##

### Identification ###

* (xxx) bits private key - suggested 16384 bits (2048 bytes)
* (xxx) bits public key - suggested 16384 bits (2048 bytes)
* Preferred encryption and decryption algorithm (to service)
* Preferred encryption and decryption algorithm (to client)
* Preferred expiration timestamp (64 bits)
* Identification user data length
* Identification user data
* Randomly generated data

### BetterAuth service ###

* Is an identification
* Hosts a list of identification authorization addresses
* Relays identification data to listed identification authorities
* Relays validity of identifications

### Identification authority ###

* Is an identification
* Hosts a list of public keys
* Relays validity of identifications
* Host encryption and decryption seeds

### Export identification to identification authorities ###

* Generate new identification with preferred configurations and user data
* Store alias without pre-alias

### Identification proxy service ###

* Is a BetterAuth service
* Relay identification as user data for generated identifications

## Data transmission ##

### Generalized data stream ###

BetterAuth uses "JavaScript Object Notation" (JSON) on top of "Transmission Control Protocol" (TCP) for data transmission

* "pk" -> Public key of sender (Base64)
* "pka" -> Public key of sender's authority (Base64)
* "pkr" -> Public key of receiver (Base64)
* "id" -> Message ID (can be a randomly generated number or string)
* "rid" -> Reply message ID (optional)
* "crypt" -> Encryption method
* "secret" -> Secret decryption key for after authority decryption
* "ctypet" -> Bottom compression type (optional)
* "ctypeb" -> Top compression type (optional)
* "msg" -> Bottom compressed, encrypted and top compressed message
* "hash" -> Checksum hash algorithm
* "checksum" -> Checksum of encoded message (Base64 ASCII to checksum)

Example:
```
#!json

{
    "pk": "...",
    "pka": "...",
    "pkr": "...",
    "id": "gBQNOaxkdIBI1AKm",
    "rid": null,
    "crypt": "Blowfish",
    "secret": "if27jSzTTuUqLdNgAFW87VAUkqmlIlOVC3hnxxyWoGW4AUVZOTCzZGmO",
    "ctypet": "None",
    "ctypeb": "None",
    "msg": "1bf8c44efcc4a80728802d27962084f4954f07b0f8fed1dfdcd61c762498170172d8deb547fb7835",
    "hash": "CRC32",
    "checksum": "fd73d819"
}
```

### Message ###

Every message uses the "JavaScript Object Notation" (JSON)

* "type" -> Message type
* "params" -> Parameters

Example:
```
#!json

{"type":"request_session", "params":[]}
```

Message types:

* "request_session" -> Requests a BetterAuth session key
* "error" -> Sends an error message
* "check_id" -> Checks ID at authority
* "id" -> Sends partial decrypted message
* "data" -> Custom data

## Used Algorithms ##

### Encryption ###

* "None" -> No encryption algorithm
* "Blowfish" -> Blowfish encryption

### Hashing ###

* "None" -> No hashing algorithm (not available for "hash" in head of generalized data stream)
* "CRC32" -> Cyclic redundancy check
* "MD5" -> MD5 hash algorithm
* "Whirlpool" -> Whirlpool hash algorithm

### Compression ###

* "None" -> No compression algorithm
* "GZIP" -> GZIP compression algorithm